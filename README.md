# my_zsh






> last update 27 May 2021






> my zsh is the zsh theme that I use for manjaro and Ubuntu
> you can find the configuration page at 
> `https://github.com/romkatv/powerlevel10k#manual` 
> this page is where I copy the config from 

---

## ======== Ubuntu 20.04,21.04 on 27 May 2021 ====================

[my_ubuntu_zsh]:https://i.ibb.co/Fw5x1zr/2021-05-27-zsh-ubuntu.png

![my ubuntu zsh][my_ubuntu_zsh]


---

## Warning from zsh after run config 

> today(27 May 2021) I got a warning in every time I open zsh like the below 
> image 


[warning_zsh]:https://i.ibb.co/WsFZ9jN/2021-05-27-zsh-warn.png

![the warning from zsh][warning_zsh]




> this warning happen because I have edit permission of the `/usr/share/zsh` 
> to 777 so it is insecure to fix this I ran 
> `sudo chmod 755 -R /usr/share/zsh` 
> and `sudo chown -R root:root /usr/share/zsh` so the problem has fixed for me.
> so if you ever have facing the same problem let try this it's maybe help!


---

> Please Note : 
> this is over 70 MB of file size you may need some time to download.




---


## update 26 May 2021 my terminal Ubuntu is gnome-terminal 

[my_zsh_2]:https://i.ibb.co/b3VVJ0r/2021-05-26-with-screenfetch.png


[gnome_terminal]:https://i.ibb.co/5sVgYLG/2021-05-26-ubuntu-gnome-terminal.png



---


# gnome-terminal

![gnome-terminal][gnome_terminal]



---

## my zsh config 26 May 2021

![my last config][my_zsh_2]




---

## my terminal is Konsole

>  this is Konsole with ZSH SHELL 29 Apr 2021. 


[my_zsh]:https://i.ibb.co/gP01hgz/2021-03-28-clear-pacman-cache.png

![my zsh terminal][my_zsh]




---


