# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi

# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi











plugins=(git zsh-syntax-highlighting zsh-autosuggestions)


autoload -Uz history-beginning-search-menu
zle -N history-beginning-search-menu
bindkey '^X^X' history-beginning-search-menu

alias ll="ls -la"
alias cl="clear; neofetch"
alias vi="nvim ."


# ======= for Ubuntu,Debian  
#alias nvar="nautilus /var/www/html"
#alias cvar="cd /var/www/html/"


# ======= manjaro
# alias nnet="nautlis /srv/http"
# alias cnet="cd /srv/http"


# ======== php artisan alias
alias ptinker="php artisan tinker"
alias pseed="php artisan db:seed"
alias pmifresh="php artisan migrate:fresh"
alias proute="php artisan route:list"
alias pwatch="npm run watch"


# just for using laravel edit on 21 Nov 21 with out this command you cannot run
# laravel new when you start new laravel project
# Add Composer bin-dir to PATH if it is installed.
    command -v composer >/dev/null 2>&1 && {
    COMPOSER_BIN_DIR=$(composer global config bin-dir --absolute 2> /dev/null)
    PATH="$PATH:$COMPOSER_BIN_DIR";
}
export PATH




