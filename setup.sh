#!/bin/bash



# copy for arcolinux 15 Oct 2021

sudo pacman -Syyu --disable-download-timeout


# config zsh 1 Mar 2021
sudo pacman -S --needed --noconfirm zsh zsh-autosuggestions zsh-completions  
sudo pacman -S --needed --noconfirm zsh-history-substring-search zsh-syntax-highlighting  
sudo pacman -S --needed --noconfirm zsh-theme-powerlevel10k neofetch dialog


# ===== source ,target
src_dir=~/my_zsh/ZSH/FOR_MANJARO/
tar_dir=/usr/share/

sudo cp -ru $src_dir/zsh $tar_dir
sudo cp -ru $src_dir/zsh-theme-powerlevel10k $tar_dir
cp $src_dir/.zshrc ~/

chsh -s /bin/zsh


function goodbye(){
    b_title="\Z3 Success! reboot to see the change"
    d_title="\Z2 Success! dear $USER please reboot your computer"
    MSG="\Z7 your zsh config file has been copied \n
    PATH is : $PATH
    you will see the change on next time you reboot \n
    you can continue on your work now just hit Enter. \n
    \Z6 Thank you for using the script \n
    \Z1 farook. "

    dialog --clear \
        --colors \
        --title "$d_title" \
        --backtitle "$b_title" \
        --msgbox "$MSG" \
        14 67
}

goodbye


